import { eventSchedules } from '../data'



export const findCurrentTimeToShow = (currentTime) => {
    // console.log("eventSchedules====>", eventSchedules)

    let item = eventSchedules.filter((e, i) => currentTime >= e.timeToShow)
    return item[item.length - 1]
    // console.log("~~~~~~~~~~~~~~~~~~~>>>", item[item.length - 1].id)
}




export const MakeGAEvent = ({ action, category, label, value = null }) => {
    window.gtag("event", action, {
        event_category: category,
        event_label: label,
        value: value
    });
};
